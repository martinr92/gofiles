# gofiles
This application creates a .go file, that contains binary data (like templates, images, ...).
Within your application, you can access to this data. This step is used to compile this data into the output binary file.

## Project
Place all files that you want to include in the application into your project folder (or subfolder):
```
/
├── main.go
├── gofiles.txt            <-- this file is described below
├── gofiles.go             <-- this file is created by this tool
└── template
    ├── index.html
    └── home.html
```

## How to use gofiles?
### Step 1: Install gofiles application
```
go get gitlab.com/martinr92/gofiles
```

### Step 2: Create a gofiles.txt
To use gofiles, simply create a text file called ```gofiles.txt``` within your project.
The content of this file must have the following format:
```
<filename>;<function name>
template/index.html;TemplateIndex
template/home.html;TemplateHome
```

### Step 3: Execute gofiles
```
cd path/to/my/project
gofiles
```

### Step 4: Use generated file
```
// GoFile<function name>
myFileContent := GoFileTemplateIndex()
```
