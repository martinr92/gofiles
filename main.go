package main

import (
	"bytes"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const outTemplate = `package #--.Package--#
#--range .Files--#
// #--.FunctionName--# returns binary file content.
func #--.FunctionName--#() []byte {
    return []byte{#--.Content--#}
}
#--end--#`

type outputStruct struct {
	Package string
	Files   []outputFile
}

type outputFile struct {
	FunctionName string
	Content      string
}

const (
	flagNameDirectory = "directory"
	flagNameFileName  = "file"
	flagNameDummyMode = "dummy"
	flagNamePackage   = "package"
	flagNameOutput    = "outputFile"
)

var (
	directory      = flag.String(flagNameDirectory, "", "path to the root directory of your project")
	fileName       = flag.String(flagNameFileName, "gofiles.txt", "name of the gofile text file within the directory (default: gofiles.txt)")
	dummyMode      = flag.Bool(flagNameDummyMode, false, "dummy file (creates just the methods without the file content)")
	packageName    = flag.String(flagNamePackage, "main", "package name of the output go file (default: main)")
	outputFileName = flag.String(flagNameOutput, "gofiles.go", "name of the output file name (default: gofiles.go)")
)

func main() {
	// validate input parameters
	validateFlags()

	// load configuration
	lines := loadAndParseGoFile()

	// load each file
	outData := outputStruct{Package: *packageName}
	for _, line := range lines {
		// skip empty lines
		if line == "" {
			continue
		}

		// split configuration line into parts
		elm := strings.Split(line, ";")

		// validate array length
		if len(elm) < 2 {
			log.Printf("invalid line content '%s'; skip this line\n", elm)
			continue
		}

		// validate configuration
		filePath := elm[0]
		functionName := "GoFile" + elm[1]
		log.Printf("start processing file %s with output function name %s...\n", filePath, functionName)

		// convert binary data for output
		fileContentString := loadAndFormatFileContent(filePath)
		log.Println("file content successfully converted")

		// build output object
		outFile := outputFile{FunctionName: functionName, Content: fileContentString}
		outData.Files = append(outData.Files, outFile)
		log.Printf("processing of function %s finished\n", functionName)
	}

	// write output file
	fileHandle, err := os.Create(filepath.Join(*directory, *outputFileName))
	if err != nil {
		log.Printf("unable to write output file '%s'\n", *outputFileName)
		log.Fatalln(err)
	}
	defer fileHandle.Close()

	// process the output using a template
	tmplate := template.New("out")
	tmplate.Delims("#--", "--#")
	tmpl := template.Must(tmplate.Parse(outTemplate))
	tmpl.Execute(fileHandle, outData)
	log.Println("gofiles successfully executed")
}

func validateFlags() {
	// parse arguments
	flag.Parse()

	// validate arguments
	if directory == nil || *directory == "" {
		// no path was given, so we're using the current working directory
		dir, err := os.Getwd()
		if err != nil {
			log.Printf("unable to detect current working directory! try to set parameter '%s'\n", flagNameDirectory)
			log.Fatalln(err)
		}
		directory = &dir
	}

	// check for dummy mode
	if isDummyMode() {
		log.Println("use dummy mode")
	}
}

func isDummyMode() bool {
	if dummyMode != nil && *dummyMode {
		return true
	}

	return false
}

func loadAndParseGoFile() []string {
	// read gofiles.txt
	fileBinary, err := ioutil.ReadFile(filepath.Join(*directory, *fileName))
	if err != nil {
		log.Println("unable to load configuration file")
		log.Fatalln(err)
	}

	// parse content of gofile.txt
	fileString := string(fileBinary)
	lines := strings.Split(fileString, "\n")
	log.Printf("configuration file loaded with %d lines\n", len(lines))
	return lines
}

func loadAndFormatFileContent(filePath string) string {
	// convert binary data for output
	var buffer bytes.Buffer
	if isDummyMode() {
		// dummy mode has just one placeholder byte
		buffer.WriteString("0x00")
	} else {
		// read file
		fileContent, err := ioutil.ReadFile(filepath.Join(*directory, filePath))
		if err != nil {
			log.Printf("unable to load file '%s' of directory '%s'\n", filePath, *directory)
			log.Fatalln(err)
		}
		log.Println("file successfully loaded")

		// format bytes
		for i, byt := range fileContent {
			if i != 0 {
				buffer.WriteString(",")
			}
			buffer.WriteString(fmt.Sprintf("%#x", byt))
		}
	}
	fileContentString := buffer.String()
	return fileContentString
}
